# Johannes' coffee cup

This was my first time participating in a CTF event, quite some time was spent learning assembly debugging with gdb (a tool I was already used to).
It's only after the event ended that I learnt about other tools such as [ghidra](https://ghidra-sre.org/), [cutter-re](https://cutter.re/), [pwntools](https://github.com/Gallopsled/pwntools#readme), [IDA](https://hex-rays.com/ida-free/), ...

## Useful gdb commands

CPU registers can be accessed using `$eax` and such. `layout asm` will show the disassembled binary while moving through it with `ni`.
Code addresses can be used in breakpoints, ... by prefixing them with `*`: `break *0x401145`. It's possible to jump directly to an address and start executing the code there using `j`.

`b` can be used to set a breakpoint (stop the execution when this location is reached), and `w` to set a watchpoint (stop the execution when this memory location is written to).

I also used `until`, and `p` and `x` to inspect the content of the stack, a memory location, ...

## Making sense of the `johannes_coffe` binary


### `gethostname`

I started by running it in gdb, and used `set follow-fork-mode child` since it was forking. I ran it in `strace` to try to find a location to break at in the child, and picked `uname`.

Starting from there, I could set a few breakpoints, and step through the code, and after some time I figured I was looking at some code checking that:
- the hostname length is 0x1e bytes
- its length xor'ed with each of its bytes computes to 0x32

I had prepended a few 'a' to my hostname to make its length correct, and was about to change it so that the above calculation results in 0x32 when 'something' reset my hostname.

I don't like this kind of changes to happen behind my back, and since I had to write some code anyway to figure out what to set my hostname to, I decided to write this code in C, and to also add a stub for libc's `gethostname` that I would `LD_PRELOAD`.

This resulted in this code:
```c
// save to gethostname.c and build with:
// $ gcc -Wall -fPIC -shared -o mygethostname.so gethostname.c
//
// test johannes_coffe with:
// $ LD_PRELOAD=./mygethostname.so ./johannes_coffe
//
#include <stddef.h>
#include <string.h>
#include <stdio.h>

// strlen(hostname) + the xor'ed chars of hostname must equal 0x32
// strlen(hostname) must be 0x1e
const char hostname[] = "Oaaa.chirashi.dolet.fergeau.eu";

int gethostname(char *name, size_t len)
{
	if (len < strlen(hostname)) {
		return 2;
	}

	strncpy(name, hostname, len);
	return 0;
}

#ifdef BUILD_MAIN
int main(int argc, char **argv) {
	unsigned int xor = strlen(hostname);
	//unsigned int xor = 0x1e;
	for (int i= 0 ; i < strlen(hostname); i++) {
		xor ^= hostname[i];
	}

	printf("strlen: %02x\n", (int)strlen(hostname));
	printf("xor: %02x\n", xor);
}
#endif
```

After going through this, I was expecting the `RH_CTF{}` flag to be close, but I was very wrong! With the above code, all I got was:
```
$ LD_PRELOAD=./mygethostname.so ./johannes_coffe
Usage: ./johannes_coffe <data>


$ LD_PRELOAD=./mygethostname.so ./johannes_coffe foo
Invalid input data size
```

Time for more debugging!


### Locating the interesting code

The hostname check is followed by some code apparently doing string copying/manipulation. It initially looked promising, but it turned out that this was copying the program args/environment, I did not look much at it. There's however a `strlen` call followed by a check that it returned `0x2e`, and this is the length of the argument `johannes_coffe` expects!

```
$ LD_PRELOAD=./mygethostname0.so ./johannes_coffe aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbccd
Working: ...
```

It's running for a (very very very) long time though. More running in gdb shows that there's a loop with many iterations (`0xa5d * 0x2e`) containing calls to `sleep`. There are also some `raise` calls with somehow random signal numbers which causes gdb's execution of the program to break/stop. I first thought this was here to annoy people using debuggers, but more on that later.

The full code for this loop is:
```
  401557:	8b 45 e8             	mov    -0x18(%rbp),%eax
  40155a:	83 e0 03             	and    $0x3,%eax
  40155d:	85 c0                	test   %eax,%eax
  40155f:	75 11                	jne    401572 <fork@plt+0x462>
  401561:	bf 45 20 40 00       	mov    $0x402045,%edi
  401566:	b8 00 00 00 00       	mov    $0x0,%eax
  40156b:	e8 00 fb ff ff       	call   401070 <printf@plt>
  401570:	eb 0a                	jmp    40157c <fork@plt+0x46c>
  401572:	bf 2e 00 00 00       	mov    $0x2e,%edi
  401577:	e8 c4 fa ff ff       	call   401040 <putchar@plt>
  40157c:	48 8b 05 3d 11 0f 00 	mov    0xf113d(%rip),%rax        # 4f26c0 <stdout@GLIBC_2.2.5>
  401583:	48 89 c7             	mov    %rax,%rdi
  401586:	e8 25 fb ff ff       	call   4010b0 <fflush@plt>
  40158b:	bf 02 00 00 00       	mov    $0x2,%edi
  401590:	e8 6b fb ff ff       	call   401100 <sleep@plt>
  401595:	8b 45 e8             	mov    -0x18(%rbp),%eax
  401598:	48 98                	cltq
  40159a:	48 8b 04 c5 c0 40 40 	mov    0x4040c0(,%rax,8),%rax
  4015a1:	00
  4015a2:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
  4015a6:	8b 45 d8             	mov    -0x28(%rbp),%eax
  4015a9:	89 05 51 11 0f 00    	mov    %eax,0xf1151(%rip)        # 4f2700
  4015af:	8b 45 dc             	mov    -0x24(%rbp),%eax
  4015b2:	89 c7                	mov    %eax,%edi
  4015b4:	e8 77 fa ff ff       	call   401030 <raise@plt>
  4015b9:	83 6d e8 01          	subl   $0x1,-0x18(%rbp)
  4015bd:	83 7d e8 00          	cmpl   $0x0,-0x18(%rbp)
  4015c1:	79 94                	jns    401557 <fork@plt+0x447>
```

and the code right after this loop is:
```
  4015c3:	ba 2e 00 00 00       	mov    $0x2e,%edx
  4015c8:	be 20 27 4f 00       	mov    $0x4f2720,%esi
  4015cd:	bf 80 26 4f 00       	mov    $0x4f2680,%edi
  4015d2:	e8 b9 fa ff ff       	call   401090 <memcmp@plt>
  4015d7:	85 c0                	test   %eax,%eax
  4015d9:	75 11                	jne    4015ec <fork@plt+0x4dc>
```

I tried to skip the loop altogether, and to directly jump to the code after it in gdb with `j *0x4015c3`.
There's a `memcmp` call comparing `0x2e` bytes at addresses `0x4f2720` and `0x4f2680`. `0x4f2720` contains a slightly garbled version of the argument I passed to `johannes_coffe`, while `0x4f2680` is filled with 0s.
With more jumping around, I was able to get `Access Granted!` to be printed, but no flag in sight!

This means more time to spend on the loop above. First things first, let's speed it up. Since we already are already `LD_PRELOAD`ing some code, we can add a stub for `sleep` and not spend minutes (or more?) waiting for a result:

```c
unsigned int sleep(unsigned int seconds)
{
	return 0;
}
```

Looking more closely at the loop above, the first half of it is just about printing stuff to the terminal, which leaves us with:
```
  401595:	8b 45 e8             	mov    -0x18(%rbp),%eax
  401598:	48 98                	cltq
  40159a:	48 8b 04 c5 c0 40 40 	mov    0x4040c0(,%rax,8),%rax
  4015a1:	00 
  4015a2:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
  4015a6:	8b 45 d8             	mov    -0x28(%rbp),%eax
  4015a9:	89 05 51 11 0f 00    	mov    %eax,0xf1151(%rip)        # 4f2700
  4015af:	8b 45 dc             	mov    -0x24(%rbp),%eax
  4015b2:	89 c7                	mov    %eax,%edi
  4015b4:	e8 77 fa ff ff       	call   401030 <raise@plt>
  4015b9:	83 6d e8 01          	subl   $0x1,-0x18(%rbp)
  4015bd:	83 7d e8 00          	cmpl   $0x0,-0x18(%rbp)
  4015c1:	79 94                	jns    401557 <fork@plt+0x447>
```

I also realised that something was amiss, I could not see anything writing to this `0x4f2720` location used as an argument to `memcmp`.
`watch *0x4f2720` in gdb helped me find the 'missing' code: the `raise` calls are not purely cosmetic/anti debugger, but there's a signal handler doing some work:

```
  401299:	55                   	push   %rbp
  40129a:	48 89 e5             	mov    %rsp,%rbp
  40129d:	89 7d fc             	mov    %edi,-0x4(%rbp)
  4012a0:	8b 05 5a 14 0f 00    	mov    0xf145a(%rip),%eax        # 4f2700
  4012a6:	48 98                	cltq
  4012a8:	0f b6 90 20 27 4f 00 	movzbl 0x4f2720(%rax),%edx
  4012af:	8b 45 fc             	mov    -0x4(%rbp),%eax
  4012b2:	89 c1                	mov    %eax,%ecx
  4012b4:	8b 05 46 14 0f 00    	mov    0xf1446(%rip),%eax        # 4f2700 <stderr@GLIBC_2.2.5+0x20>
  4012ba:	31 ca                	xor    %ecx,%edx
  4012bc:	48 98                	cltq
  4012be:	88 90 20 27 4f 00    	mov    %dl,0x4f2720(%rax)
  4012c4:	90                   	nop
  4012c5:	5d                   	pop    %rbp
  4012c6:	c3                   	ret
```

### Putting (almost) everything together

I know had located most of the useful code, and after spending more time stepping through it, printing values, ... I finally what the code does, and hopefully how to get the flag:
- most of the `johannes_coffe` binary is a big data table (from file offset `0x30c0` to `0xf1670`)
- this table contains 0x170 * 0x2e 8 bytes entries
- first 4 bytes are an index in the 0x2e byte array which `argv[1]` is
- next 4 bytes are a value to `xor` the `argv[1][index]` with

Looking at a hex dump of the data, none of the values are higher than 0x2e or 0xff, so what is only a theory at this point is at least consistent:
```
000030c0: 2700 0000 3c00 0000 1a00 0000 0800 0000  '...<...........
000030d0: 2500 0000 2c00 0000 2000 0000 3300 0000  %...,... ...3...
000030e0: 0e00 0000 2f00 0000 0f00 0000 3b00 0000  ..../.......;...
000030f0: 0300 0000 3500 0000 1000 0000 0700 0000  ....5...........
00003100: 0000 0000 2800 0000 2200 0000 0d00 0000  ....(...".......
[...]
```
(x86\_64 CPUs are [little-endian](https://en.wikipedia.org/wiki/Endianness#Overview) so `2700 0000` is `0x27` when read as a 32-bit int value)

If we write code to iterate over this data table, starting with a 0x2e byte long array filled with 0s, I'm quite sure we'll be getting what we need, this could even directly give us the flag....

At this point, I had already spent far too much time on this problem, and did not feel like writing this code. More importantly, I had to leave for lunch, and then for PTOs... Too bad for this challenge...


### Wrapping things up

This was not over yet, as I had some ideas over lunch. At that point, I was quite familiar with this code and with running it in gdb. It's already doing the loop + computation that I needed, so I could as well make use of it! My initial idea was to break in gdb before the loop, to fill argv[1] with 0s, and to print its content at the end of the loop. I tried this after the CTF end, but this was not the magic bullet I hoped: the `raise` calls cause constant interruptions in gdb, and I did not manage to tell gdb to ignore all of them. And the loop had too many iterations that it was not really workable to keep telling gdb to continue until the loop was done.

I was once more back to the drawing board. I realized that instead of using gdb, I could add more code to my `LD_PRELOAD` wrapper for the same purpose: the program ends with a `memcmp` call. The first argument of this call was initially argv[1], but with the transformation described before applied to it. If a write a `memcmp` wrapper which takes the first `memcmp` argument, and `xor` it with `argv[1]`, I will get the result that I want without writing any code.

I tried this, but did not get anything useful. At this point, I was thinking this `memcmp` expects both of its args to be 0 as the second arg was filled with 0s when I checked it with gdb. However, dumping it from the `LD_PRELOAD`'ed code was showing non-0 content, which was confusing. I tried quite a few things, I also had read [this write-up](https://github.com/r00ta/myWriteUps/blob/master/RH_CTF_2022/README.md) in the mean time which mentions a magic string I hadn't seen yet, ... I finally tried to `xor` together both `memcmp` args and `argv[1]`, and lo and behold! My code printed `RH_CTF{Th1s_i5_4_V3rY_w311_hIdd3N_s3cr37_info}`


My final code:
```c
// save to gethostname.c and build with:
// $ gcc -Wall -fPIC -shared -o mygethostname.so gethostname.c
//
// test johannes_coffe with:
// $ LD_PRELOAD=./mygethostname.so ./johannes_coffe aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbccc
//
// The argument must be 0x2e bytes long and must match the argv1 constant below.
//
#include <stddef.h>
#include <string.h>
#include <stdio.h>

const char *argv1 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbccc";

// strlen(hostname) + the xor'ed chars of hostname must equal 0x32
// strlen(hostname) must be 0x1e
const char hostname[] = "Oaaa.chirashi.dolet.fergeau.eu";

int gethostname(char *name, size_t len)
{
	if (len < strlen(hostname)) {
		return 2;
	}

	strncpy(name, hostname, len);
	return 0;
}

unsigned int sleep(unsigned int seconds)
{
	return 0;
}

static void print_string(const char *s, size_t n)
{
	for (int i = 0; i < n; i++) {
		printf("%c", s[i]);
	}
	printf("\n");
}

int memcmp(const void *s1, const void *s2, size_t n)
{
	char result[n];
	for (int i = 0; i < n; i++) {
		result[i] = ((char *)s2)[i] ^ argv1[i];
		result[i] ^= ((char *)s1)[i];
	}
	print_string(result, n);
	return 0;
}



#ifdef BUILD_MAIN
int main(int argc, char **argv) {
	unsigned int xor = strlen(hostname);
	//unsigned int xor = 0x1e;
	for (int i= 0 ; i < strlen(hostname); i++) {
		xor ^= hostname[i];
	}

	printf("strlen: %02x\n", (int)strlen(hostname));
	printf("xor: %02x\n", xor);
}
#endif
```

```
$ LD_PRELOAD=./mygethostname.so ./johannes_coffe aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbccc
Working:
RH_CTF{Th1s_i5_4_V3rY_w311_hIdd3N_s3cr37_info}
Access Granted!
```
