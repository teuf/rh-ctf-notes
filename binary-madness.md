# Binary Madness: The Latest Social Media Craze!

This challenge consists of a binary:

```
$ file binary
binary: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=6ac6c8dd393f510172aafa8d4c1b77ef4a2931fb, for GNU/Linux 3.2.0, stripped

$ chmod 755 binary  && ./binary
Nope

$ ./binary RH_CTF
Closer, but no
```

So far, no big surprise, this is an x86_64 binary, so it can be run easily, it's stripped, and it seems to expect the flag as an argument.

Looking at `strings` output, there is a little bit of code in plain text for some reason:

```
$ strings ./binary
[...]
retval == -1 || ((retval & 0x1F) == retval)
block >= 0 && block < 8
len >= 0 && len <= 5
coded && plain
octet < 4
decode_char
get_octet
get_offset
encode_sequence
decode_sequence
Nope
Closer, but no
Well done! Validate with the flag: %s
[...]
```


Taking a closer look with `hexedit` shows that all these strings are next to each other in the file:

```
00002010   3D 62 61 73  65 33 32 2E  63 00 00 00  00 00 00 00  =base32.c.......
00002020   72 65 74 76  61 6C 20 3D  3D 20 2D 31  20 7C 7C 20  retval == -1 ||
00002030   28 28 72 65  74 76 61 6C  20 26 20 30  78 31 46 29  ((retval & 0x1F)
00002040   20 3D 3D 20  72 65 74 76  61 6C 29 00  62 6C 6F 63   == retval).bloc
00002050   6B 20 3E 3D  20 30 20 26  26 20 62 6C  6F 63 6B 20  k >= 0 && block
00002060   3C 20 38 00  6C 65 6E 20  3E 3D 20 30  20 26 26 20  < 8.len >= 0 &&
00002070   6C 65 6E 20  3C 3D 20 35  00 63 6F 64  65 64 20 26  len <= 5.coded &
00002080   26 20 70 6C  61 69 6E 00  6F 63 74 65  74 20 3C 20  & plain.octet <
00002090   34 00 00 00  00 00 00 00  64 65 63 6F  64 65 5F 63  4.......decode_c
000020A0   68 61 72 00  00 00 00 00  67 65 74 5F  6F 63 74 65  har.....get_octe
000020B0   74 00 00 00  00 00 00 00  67 65 74 5F  6F 66 66 73  t.......get_offs
000020C0   65 74 00 00  00 00 00 00  00 00 00 00  00 00 00 00  et..............
000020D0   65 6E 63 6F  64 65 5F 73  65 71 75 65  6E 63 65 00  encode_sequence.
000020E0   64 65 63 6F  64 65 5F 73  65 71 75 65  6E 63 65 00  decode_sequence.
000020F0   4E 6F 70 65  00 43 6C 6F  73 65 72 2C  20 62 75 74  Nope.Closer, but
00002100   20 6E 6F 00  00 00 00 00  57 65 6C 6C  20 64 6F 6E   no.....Well don
00002110   65 21 20 56  61 6C 69 64  61 74 65 20  77 69 74 68  e! Validate with
00002120   20 74 68 65  20 66 6C 61  67 3A 20 25  73 0A 00 45   the flag: %s..E
00002130   72 72 72 2C  20 6E 6F 00  01 1B 03 3B  8C 00 00 00  rrr, no....;....
```


This starts with `base32.c`, I know of base64, but base32?

```
$ base<TAB><TAB>
base32    base64    basename  basenc
```


Interesting, there's a `base32` tool, and it works in a similar way to `base64`:

```
$ echo RH_CTF | base32
KJEF6Q2UIYFA====
```

Let's grep for this in `binary`, it most likely won't return any results, but this is a quick test:

```
$ strings binary |grep KJEF6Q2
KJEF6Q2UIZ5VSMDVL5BTI3SUL5ZTGM27JUZV6YZUJZPXST3VPU======
```

Wow, looks like a lucky day! Are we close to solving this challenge?

```
$ echo -n  'KJEF6Q2UIZ5VSMDVL5BTI3SUL5ZTGM27JUZV6YZUJZPXST3VPU======' |base32 -d
RH_CTF{Y0u_C4nT_s33_M3_c4N_yOu}

$ ./binary RH_CTF{Y0u_C4nT_s33_M3_c4N_yOu}
Well done! Validate with the flag: RH_CTF{Y0u_C4nT_s33_M3_c4N_yOu}
```
